﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;

namespace DataRepository.Repositorys
{
    public interface IAccountOpeningRepository : IMongoRepository<AccountOpening>
    {
        Task<List<AccountOpening>> FindAllAccounts();
        
        Task<List<AccountOpening>> FindAccount(string accountNumber);
    }
}