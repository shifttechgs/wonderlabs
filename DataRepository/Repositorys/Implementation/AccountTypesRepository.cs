﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;
using MongoDB.Driver;

namespace DataRepository.Repositorys
{
    public class AccountTypesRepository : MongoRepository<AccountTypes>, IAccountTypesRepository
    {
        public AccountTypesRepository(IMongoDbSettings settings) : base(settings)
        {
        }

        public async Task<List<AccountTypes>> FindAllAccountTypes()
        {
            var result = _collection.Find(x => true);
            return result.ToList();
        }
    }
}