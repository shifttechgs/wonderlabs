﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace DataRepository.Repositorys
{
    public class TransactionsRepository: MongoRepository<Transactions>, ITransactionsRepository
    {
        public TransactionsRepository(IMongoDbSettings settings) : base(settings)
        {
        }

        public async Task<List<Transactions>> FindAllTransactions()
        {
            var result = _collection.Find(x => true);
            return result.ToList();
        }
        
        public async  Task<List<Transactions>> FindAccountTransactions( string accountId)
        {
           var result = _collection.AsQueryable().Where(x => x.AccountId == accountId).ToList();
           return result;
        }
        
        
    }
}