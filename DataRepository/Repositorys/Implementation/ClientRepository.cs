﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Identity;
using Models.Domain;
using Models.ViewModels;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;


namespace DataRepository.Repositorys
{
    public class ClientRepository: MongoRepository<Client>, IClientRepository
    {
        
        public ClientRepository(IMongoDbSettings settings) : base(settings)
        {
          
        }


        public IEnumerable<Client> FindByIds(List<ObjectId> ids)
        {
            return _collection.Find(x => ids.Contains(x.Id)).ToEnumerable();
        }
     
        public IMongoQueryable<Client> GetClientsByAdminId(ObjectId adminUser)
        {
          
            return _collection.AsQueryable().Where(x => x.AdminUser == adminUser );
        }

         

    }
}