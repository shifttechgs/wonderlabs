﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace DataRepository.Repositorys
{
    public class AccountOpeningRepository : MongoRepository<AccountOpening>, IAccountOpeningRepository
    {
        public AccountOpeningRepository(IMongoDbSettings settings) : base(settings)
        {
        }

        public async Task<List<AccountOpening>> FindAllAccounts()
        {
            var result = _collection.Find(x => true);
            return result.ToList();
        }

        public async Task<List<AccountOpening>> FindAccount(string accountNumber)
        {
            var result = _collection.AsQueryable().Where(x => x.AccountNumber == accountNumber).ToList();
            return result;
        }
    }
}