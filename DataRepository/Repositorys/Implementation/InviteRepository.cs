﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Identity;
using Models.Domain;
using Models.ViewModels;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;


namespace DataRepository.Repositorys
{
    public class InviteRepository: MongoRepository<Invite>, IInviteRepository
    {
        
        public InviteRepository(IMongoDbSettings settings) : base(settings)
        {


        }
        

        public IMongoQueryable<Invite> getInvitesByClient(ObjectId clientId)
        {
            return _collection.AsQueryable().Where(x => x.ClientId == clientId);
        }
    }
}