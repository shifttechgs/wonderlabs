﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;
using MongoDB.Bson;
using MongoDB.Driver;

namespace DataRepository.Repositorys
{
    public class UserRepository: MongoRepository<User>, IUserRepository
    {
        public UserRepository(IMongoDbSettings settings) : base(settings)
        {
            
        }

        public async Task<List<ObjectId>> FindUsersClients(ObjectId userId)
        {
            var result = await _collection.Find(x => x.Id == userId).FirstOrDefaultAsync();
            return result.Clients;
        }
        
    }
}