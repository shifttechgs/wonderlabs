﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;
using MongoDB.Bson;
using MongoDB.Driver.Linq;

namespace DataRepository.Repositorys
{
    public interface ITransactionsRepository : IMongoRepository<Transactions>
    {
        Task<List<Transactions>> FindAllTransactions();

        Task<List<Transactions>> FindAccountTransactions(string accountId);
            
    }
}