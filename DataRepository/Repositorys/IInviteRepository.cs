﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Models.Domain;
using Models.ViewModels;
using MongoDB.Bson;
using MongoDB.Driver.Linq;

namespace DataRepository.Repositorys
{
    public interface IInviteRepository :IMongoRepository<Invite>
    {
        
      

      IMongoQueryable<Invite> getInvitesByClient(ObjectId clientId);

    }
}