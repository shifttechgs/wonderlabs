﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;
using MongoDB.Bson;
using Microsoft.AspNetCore.Identity;
using Models.Domain;
using Models.ViewModels;

namespace DataRepository.Repositorys
{
    public interface IUserRepository: IMongoRepository<User>
    {
        Task<List<ObjectId>> FindUsersClients(ObjectId userId);
        
    }
}