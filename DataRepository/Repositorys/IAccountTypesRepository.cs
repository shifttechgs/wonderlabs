﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Domain;

namespace DataRepository.Repositorys
{
    public interface IAccountTypesRepository : IMongoRepository<AccountTypes>
    {
        Task<List<AccountTypes>> FindAllAccountTypes();
    }
}