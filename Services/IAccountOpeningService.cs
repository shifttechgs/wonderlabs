﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.ViewModels;

namespace Services
{
    public interface IAccountOpeningService
    {
        public Task<List<AccountOpeningViewModel>> GetAllAccounts(string accountNumber);
       // public Task<List<AccountOpeningViewModel>> GetAccount(string id);
        
        public Task<List<AccountOpeningViewModel>> GetAllAccounts(); // Admin 
    }
}