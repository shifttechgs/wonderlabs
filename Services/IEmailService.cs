﻿using System.Threading.Tasks;

namespace Services
{
    public interface IEmailService
    {
       // Task SendEmail(string emailAddress, string templateName, object data);
       Task SendEmail(string emailAddress, string templateName, string userName, string adminName, string url);
    }
}