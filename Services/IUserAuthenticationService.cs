﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Models.Domain;
using Models.ViewModels;

namespace Services
{
    public interface IUserAuthenticationService
    {
        Task<LoggedInUserViewModel> Authenticate(LoginViewModel model);
        Task<IdentityResult> RegisterUser(RegisterViewModel model);
        Task<IdentityResult> RegisterUserWithInvite(RegisterViewModel model, Guid token);
      //   Task<IdentityResult> ClientRegisterUser(ClientRegisterViewModel model);
        Task<IdentityResult> UpdateUser(UserViewModel model);

        Task<InsertRoleWithClaimsViewModel> InsertRoleAndPermissions(
            InsertRoleWithClaimsViewModel roleWithClaimsViewModel);

        Task<string> CreateJwt(User user);
       Task ConfirmEmailAsync(string id, string token);
     
        Task SendResetPasswordLink(string emailAddress);
        Task<IdentityResult> ResetPasswordAsync(ResetPasswordViewModel model);

        Task<Invite> SendInviteEmail(InviteViewModel inviteViewModel);

      //  Task ChangeStatusAsync(string userId, string token);
      Task<UserViewModel> GetUserByEmail(string email);
    }
}