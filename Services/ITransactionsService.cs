﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models.ViewModels;

namespace Services
{
    public interface ITransactionsService
    {
        public Task<List<TransactionsViewModel>> GetTransactions(string accountId);
        public Task<List<TransactionsViewModel>> GetAllTransactions(string accountId);
        public Task<List<TransactionsViewModel>> GetAllTransactions(); // Admin 
        public Task<TransactionsViewModel> GetTransaction(string id);
    }
}