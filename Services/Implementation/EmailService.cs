﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Models.Domain;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Services.Implementation
{
    public class EmailService : IEmailService
    {

        //private readonly ISendGridSettings _sendGridSettings;
        //private readonly SendGridClient _client;
        private readonly ISMTPSettings _smtpSettings;
        
        public EmailService(ISendGridSettings sendGridSettings,ISMTPSettings smtpSettings)
        {
            _smtpSettings = smtpSettings;
           // _sendGridSettings = sendGridSettings;
          //  this._client = new SendGridClient(_sendGridSettings.ApiKey);
        }


        public async Task SendEmail(string emailAddress, string templateName, string userName, string adminName, string url)
       //public async Task SendEmail(string emailAddress,  string templateName, object data)
        {
            // var template = _sendGridSettings.Templates.FirstOrDefault(x => x.Name == templateName);
            //
            // var email = new SendGridMessage();
            // email.SetFrom(new EmailAddress(_sendGridSettings.FromEmailAddress, _sendGridSettings.FromName));
            // email.AddTo(new EmailAddress(emailAddress));
            // email.TemplateId = template.TemplateId;
            // email.SetTemplateData(data);
            // var result = await _client.SendEmailAsync(email);
            
            
            
            var smtpClient = new SmtpClient(_smtpSettings.SMTPAddress)
            {
                Port = _smtpSettings.Port,
                Credentials = new NetworkCredential(_smtpSettings.Email, _smtpSettings.Password),
                EnableSsl = true,
            };
            
            var body = "";
            var subject = "";
            
            switch (templateName)
            {
                case "Invite User":
                    body =
                        $"<div style=\"margin: auto; max-width: 500px;\"> <h2>Hi {userName}, </h2><p> {adminName} has invited you to join the EWT Data Bank</p><br/><a href={url} target=\"_blank\" style=\"padding: 8px 12px;border-radius: 5px;font-family: Helvetica, Arial, sans-serif;font-size: 14px;text-decoration: none;font-weight:bold;display: inline-block; background-color: black; color: white;\" href={url} >Join</a></div>";
                    subject = "Invite User";
                    break;
                case "Reset Password":
                    body =   $"<div style=\"margin: auto; max-width: 500px;\"> <h2>Hi {userName}, </h2><p> Please select the button below to reset your password!. <br /></p><a href={url} target=\"_blank\" style=\"padding: 8px 12px;border-radius: 5px;font-family: Helvetica, Arial, sans-serif;font-size: 14px;text-decoration: none;font-weight:bold;display: inline-block; background-color: black; color: white;\" href={url} >Reset your password</a></div>";;
                    subject = "Forgot Password";
                    break;
                
                case "Confirm Account":
                    body =   $"<div style=\"margin: auto; max-width: 500px;\"> <h2>Hi {userName}, </h2><p> Please select the button below to confirm your account!. <br /></p><a href={url} target=\"_blank\" style=\"padding: 8px 12px;border-radius: 5px;font-family: Helvetica, Arial, sans-serif;font-size: 14px;text-decoration: none;font-weight:bold;display: inline-block; background-color: black; color: white;\" href={url} >Confirm your account</a></div>";;
                    subject = "Forgot Password";
                    break;
            }
            
            var mailMessage = new MailMessage
            {
                From = new MailAddress(_smtpSettings.Email),
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            };
            mailMessage.To.Add(emailAddress);
            smtpClient.Send(mailMessage);

        }


       
    }
    
}