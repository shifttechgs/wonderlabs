﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataRepository.Repositorys;
using Models.Domain;
using Models.ViewModels;

namespace Services.Implementation
{
    public class AccountOpeningService : IAccountOpeningService
    {
        private readonly IAccountOpeningRepository _accountOpeningRepository;


        public AccountOpeningService(IAccountOpeningRepository accountOpeningRepository)
        {
            _accountOpeningRepository = accountOpeningRepository;
        }


        public async Task<List<AccountOpeningViewModel>> GetAllAccounts(string accountNumber)
        {
            var list = new List<AccountOpeningViewModel>();
            var accounts = _accountOpeningRepository.AsQueryable().Where(x => x.AccountNumber == accountNumber);

            foreach (var account in accounts)
            {
                list.Add(new AccountOpeningViewModel
                {
                    AccountName = account.AccountName,
                    AccountBalance = account.AccountBalance,
                    AccountType = account.AccountType,
                    AccountNumber = account.AccountNumber,
                    DateOpened = account.DateOpened.ToString(),
                });
            }

            return list;
        }

        public Task<List<AccountOpeningViewModel>> GetAllAccounts()
        {
            throw new System.NotImplementedException();
        }


        public async Task<List<AccountOpeningViewModel>> GetAllAccountss()
        {
            var list = new List<AccountOpeningViewModel>();
            var accounts = _accountOpeningRepository.AsQueryable().ToList();


            foreach (var account in accounts)
            {
                list.Add(new AccountOpeningViewModel
                {
                    AccountName = account.AccountName,
                    AccountBalance = account.AccountBalance,
                    AccountType = account.AccountType,
                    AccountNumber = account.AccountNumber,
                    DateOpened = account.DateOpened.ToString(),
                });
            }

            return list;
        }
    }
}