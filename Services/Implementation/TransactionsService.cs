﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataRepository.Repositorys;
using Models.Domain;
using Models.ViewModels;

namespace Services.Implementation
{
    public class TransactionsService : ITransactionsService
    {
        private readonly ITransactionsRepository _transactionsRepository;


        public TransactionsService(ITransactionsRepository transactionsRepository)
        {
            _transactionsRepository = transactionsRepository;
        }


        public Task<List<TransactionsViewModel>> GetTransactions(string accountId)
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<TransactionsViewModel>> GetAllTransactions(string accountId)
        {
            var list = new List<TransactionsViewModel>();
            var transactions = _transactionsRepository.AsQueryable().Where(x => x.AccountId == accountId);

            foreach (var transaction in transactions)
            {
                list.Add(new TransactionsViewModel
                {
                    AccountId = transaction.AccountId,
                    AccountBalance = transaction.AccountBalance,
                    UserTransaction = transaction.UserTransaction,
                    LastDateOfTransaction = transaction.LastDateOfTransaction
                });
            }

            return list;
        }


        public async Task<List<TransactionsViewModel>> GetAllTransactions()
        {
            var list = new List<TransactionsViewModel>();
            var transactions = _transactionsRepository.AsQueryable().ToList();


            foreach (var transaction in transactions)
            {
                list.Add(new TransactionsViewModel
                {
                    AccountId = transaction.AccountId,
                    AccountBalance = transaction.AccountBalance,
                    UserTransaction = transaction.UserTransaction,
                    LastDateOfTransaction = transaction.LastDateOfTransaction
                });
            }

            return list;
        }


        public async Task<TransactionsViewModel> GetTransaction(string transactionId)
        {
            var transaction = await _transactionsRepository.FindByIdAsync(transactionId);

            return new TransactionsViewModel
            {
                AccountId = transaction.AccountId,
                AccountBalance = transaction.AccountBalance,
                UserTransaction = transaction.UserTransaction,
                LastDateOfTransaction = transaction.LastDateOfTransaction
            };
        }
    }
}