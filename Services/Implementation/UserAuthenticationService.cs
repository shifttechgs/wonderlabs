﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Models.ViewModels;
using System.Security.Claims;
using System.Text;
using System.Web;
using AutoMapper;
using DataRepository.Repositorys;
using Microsoft.IdentityModel.Tokens;
using Models.Domain;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;

namespace Services.Implementation
{
    public class UserAuthenticationService : IUserAuthenticationService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly ILogger _logger;
        private RoleManager<Role> _roleManager;
        private IAppSettings _appSettings;
        private IEmailService _mailService;
        private readonly IInviteRepository _inviteRepository;
        private readonly IClientRepository _clientRepository;
        private readonly IMapper _mapper;

        public UserAuthenticationService(RoleManager<Role> roleManager, SignInManager<User> signInManager,
            UserManager<User> userManager, IInviteRepository inviteRepository,
            ILogger<UserAuthenticationService> logger,
            IAppSettings appSettings,
            IEmailService mailService,
            IMapper mapper,IClientRepository clientRepository)
        {
            _signInManager = signInManager;
            _logger = logger;
            _userManager = userManager;
            _roleManager = roleManager;
            _appSettings = appSettings;
            _mailService = mailService;
            _inviteRepository = inviteRepository;
            _mapper = mapper;
            _clientRepository = clientRepository;
        }

        public async Task<LoggedInUserViewModel> Authenticate(LoginViewModel model)
        {
            try
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe,
                    lockoutOnFailure: false);

                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(model.UserName);
                    // return null if user not found
                    if (user == null)
                        return null;

                    var loggedInUser = new LoggedInUserViewModel()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        UserName = user.UserName,
                        Token = await CreateJwt(user)
                    };
                    return loggedInUser;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<IdentityResult> RegisterUser(RegisterViewModel model)
        {
            var user = new User
            {
                UserName = model.UserName,
                Email = model.EmailAddress
            };

            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded)
            {
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var validEmailToken = HttpUtility.UrlEncode(code);
                string url = $"{_appSettings.WebUrl}/account/verifyAccount/{user.Id}/{validEmailToken}";
                
                var userName = $"{user.UserName} {user.Email}"; 
                await _mailService.SendEmail(user.Email, "Confirm Account", userName, null, url);

                
                // await _mailService.SendEmail(user.Email, EmailTemplateHelper.ConfirmEmail,
                //     new {user = model.UserName, confirm_email_link = url});
            }

            return result;
        }
        
        public async Task<IdentityResult> RegisterUserWithInvite(RegisterViewModel model, Guid token)
        {
          
            

            //Find invite by token
            var invite = await _inviteRepository.FindOneAsync(x => x.Token == token);


            //Validations
            //Check if invite has not been used

            if (invite.Accepted)
            {
                //Invite has been used/accepted already
                throw new Exception("Invite has already been used!");
            }
          
            //Check if invite has expired?
            if (invite.CreatedAt.AddDays(2) < DateTime.UtcNow)
            {
                //Invite has been used/accepted already
                throw new Exception("Invite has expired!");
            }
            
            //Check if user already exists on the database

            var user = await _userManager.FindByEmailAsync(model.EmailAddress);

            if (user != null)
            {
                throw new Exception("User already exists");
            }
            
            //Creates user on DB
            
            var identityResult = await RegisterUser(model);
            
            //Find new user in database
            user = await _userManager.FindByEmailAsync(model.EmailAddress);
            if (user.Clients == null)
            {
                user.Clients = new List<ObjectId>();
            }
            
            //Find the client
            //var client = await _clientRepository.FindByIdAsync(invite.ClientId.ToString());
            //var client = _clientRepository.FindOne(x=> x.Id == invite.ClientId);
            
            //Add client to the user
            user.Clients.Add(invite.ClientId);
            await _userManager.UpdateAsync(user);
            
            
            //Update invite table
            invite.Accepted = true;
            await _inviteRepository.ReplaceOneAsync(invite);
            
            return identityResult;
        }

     

        public async Task<IdentityResult> UpdateUser(UserViewModel model)
        {
            var user = await _userManager.FindByIdAsync(model.Id);

            if (user != null)
            {
                user.UserName = model.UserName;
                user.Email = model.EmailAddress;
                var userRoles = await _userManager.GetRolesAsync(user);
                var rolesToAssign = new List<string>();
                var rolesToRemove = new List<string>();

                foreach (var roleViewModel in model.Roles)
                {
                    var role = await _roleManager.FindByIdAsync(roleViewModel.Id);
                    if (!userRoles.Contains(role.Name))
                    {
                        rolesToAssign.Add(role.Name);
                    }
                }

                var allUnassignedRoles = _roleManager.Roles.ToList()
                    .Where(x => model.Roles.All(y => y.Id != x.Id.ToString()));
                foreach (var role in allUnassignedRoles)
                {
                    if (userRoles.Contains(role.Name))
                    {
                        rolesToRemove.Add(role.Name);
                    }
                }

                var removeRolesResult = await _userManager.RemoveFromRolesAsync(user, rolesToRemove);
                var addRolesResult = await _userManager.AddToRolesAsync(user, rolesToAssign);

                if (!removeRolesResult.Succeeded || !addRolesResult.Succeeded)
                {
                    return IdentityResult.Failed(removeRolesResult.Errors.Union(addRolesResult.Errors).ToArray());
                }
            }

            return IdentityResult.Success;
        }

        public async Task<InsertRoleWithClaimsViewModel> InsertRoleAndPermissions(
            InsertRoleWithClaimsViewModel roleWithClaimsViewModel)
        {
            if (string.IsNullOrEmpty(roleWithClaimsViewModel.Role.Id))
            {
                //New Role
                var role = new Role()
                {
                    Description = roleWithClaimsViewModel.Role.Description,
                    Name = roleWithClaimsViewModel.Role.Name
                };

                var result = await _roleManager.CreateAsync(role);
                
                if (!result.Succeeded)
                {
                    roleWithClaimsViewModel.IdentityResult = result;
                    return roleWithClaimsViewModel;
                }

                role = await _roleManager.FindByNameAsync(role.Name);
                
                role.Claims ??= new List<IdentityRoleClaim<string>>();

                foreach (var claim in roleWithClaimsViewModel.Claims)
                {
                    var claimsResult =
                        await _roleManager.AddClaimAsync(role, new Claim(claim.ClaimType, claim.ClaimValue));

                    if (!result.Succeeded)
                    {
                        roleWithClaimsViewModel.IdentityResult = claimsResult;
                        return roleWithClaimsViewModel;
                    }
                }
            }
            else
            {
                //Editing
                var existingRole = await _roleManager.FindByIdAsync(roleWithClaimsViewModel.Role.Id);
                var existingClaims = await _roleManager.GetClaimsAsync(existingRole);

                existingRole.Name = roleWithClaimsViewModel.Role.Name;
                existingRole.Description = roleWithClaimsViewModel.Role.Description;


                foreach (var claim in existingClaims)
                {
                    if (roleWithClaimsViewModel.Claims.All(x => x.ClaimValue != claim.Value))
                    {
                        await _roleManager.RemoveClaimAsync(existingRole, claim);
                    }
                }

                foreach (var claim in roleWithClaimsViewModel.Claims)
                {
                    if (existingClaims.All(x => x.Value != claim.ClaimValue))
                    {
                        await _roleManager.AddClaimAsync(existingRole, new Claim(claim.ClaimType, claim.ClaimValue));
                    }
                }

                await _roleManager.UpdateAsync(existingRole);
            }

            roleWithClaimsViewModel.IdentityResult = IdentityResult.Success;

            return roleWithClaimsViewModel;
        }

        public async Task<string> CreateJwt(User user)
        {
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var userRoleClaims = (await _userManager.GetClaimsAsync(user)).ToList();

            claims.AddRange(userRoleClaims);

            var userRoles = await _userManager.GetRolesAsync(user);
            claims.AddRange(userRoles.Select(x => new Claim(ClaimTypes.Role, x)).ToList());

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task ConfirmEmailAsync(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task SendResetPasswordLink(string emailAddress)
        {
            var user = await _userManager.FindByEmailAsync(emailAddress);
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);
            var validToken = HttpUtility.UrlEncode(token);

            string url = $"{_appSettings.WebUrl}/account/ResetPassword/{emailAddress}/{validToken}";

            var userName = $"{user.UserName} {user.Email}"; 
            await _mailService.SendEmail(user.Email, "Reset Password", userName, null, url);

            // await _mailService.SendEmail(user.Email, EmailTemplateHelper.ForgotPassword,
            //     new {user = user.UserName, reset_password_link = url});
        }

        // public async Task<IdentityResult> ResetPasswordAsync(ResetPasswordViewModel model)
        // {
        //     var user = await _userManager.FindByEmailAsync(model.EmailAddress);
        //
        //
        //     var decodedToken = WebEncoders.Base64UrlDecode(model.Token);
        //     string normalToken = Encoding.UTF8.GetString(decodedToken);
        //
        //     var result = await _userManager.ResetPasswordAsync(user, normalToken, model.NewPassword);
        //
        //     return result;
        // }
        
        public async Task<IdentityResult> ResetPasswordAsync(ResetPasswordViewModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.EmailAddress);
            return await _userManager.ResetPasswordAsync(user, model.Token, model.NewPassword);
        }


        public async Task<Invite> SendInviteEmail(InviteViewModel inviteViewModel)
        {
            Invite invite = _mapper.Map<Invite>(inviteViewModel);
          //  var user = await _inviteRepository.FindOneByA(inviteViewModel.Email);
            
            
            var token = Guid.NewGuid();
            invite.Token = token;
            invite.ClientId = new ObjectId(inviteViewModel.ClientId);
            await _inviteRepository.InsertOneAsync(invite);

            // string url = $"{_appSettings.WebUrl}/account/accept/{HttpUtility.UrlEncode(inviteViewModel.Email)}/{HttpUtility.UrlEncode(invite.Token.ToString())}";
            //
            // await _mailService.SendEmail(inviteViewModel.Email, EmailTemplateHelper.InviteClient,
            //     new {user = inviteViewModel.FirstName, admin = "Bryan Nicholson", send_invite_link = url});

            return invite;
        }
        
        public async Task<UserViewModel> GetUserByEmail(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            

            return new UserViewModel()
            {
                Id = user.Id.ToString(),
                UserName = user.UserName,
               
                EmailAddress = user.Email
               
            };
        }


       
    }
}