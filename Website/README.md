# BasePlatform

## Angular Project Setup 
### Install Code Gen Dependencies 
Install [OpenAPI Generator](https://github.com/OpenAPITools/openapi-generator) for full instructions

Run `npm install @openapitools/openapi-generator-cli -g`

### Generate the client 
Download the latest [swagger.json](https://localhost:5001/swagger/v1/swagger.json) file and save it in the Website folder for code generation

Run `npm run generate-client`

### Development server
Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## General Notes

### Included Library's for design and layout
 - [Bootstrap grid](https://getbootstrap.com/docs/4.1/layout/grid/) 
 - [Bootstrap utilities](https://getbootstrap.com/docs/4.1/layout/utilities-for-layout/) 
 - [Bootstrap reboot](https://getbootstrap.com/docs/4.1/content/reboot/)
 - [Angular Material](https://material.angular.io/components/categories) 

This project use the Grid layout system from Bootstrap 4, it does not include the entire Bootstrap style sheet but rather just some useful parts.
Angular Material is used for components 
