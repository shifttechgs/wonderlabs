import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {AuthenticationService as APIAuthenticationService} from 'api-client';
import {IdentityResult} from '../../../projects/api-client/src';
import {ActivatedRoute, Router} from '@angular/router';
import {MatchPassword} from '../models/validation-helpers';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  token: string;
  public pwdPattern = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#$^+=!*()@%&]).{8,}$';
  public changeForm = this.fb.group({
    emailAddress: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.pattern(this.pwdPattern)]],
    ConfirmPassword: ['', [Validators.required]]
  }, {
    validator: MatchPassword('password', 'ConfirmPassword')
  });

  public get password(): AbstractControl {
    return this.changeForm.get('password');
  }

  public get confirmPassword(): AbstractControl {
    return this.changeForm.get('ConfirmPassword');
  }


  constructor(private toastr: ToastrService,
              private authService: APIAuthenticationService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) {
    this.changeForm.patchValue({
      emailAddress: this.route.snapshot.paramMap.get('email')
    });
    this.token = this.route.snapshot.paramMap.get('token');
  }

  ngOnInit(): void {
  }

  public async resetPassword(): Promise<void> {
    if (this.changeForm.valid) {
      const result: IdentityResult = await this.authService.apiAuthenticationResetPasswordPost({
        emailAddress: this.changeForm.value.emailAddress,
        confirmPassword: this.changeForm.value.ConfirmPassword,
        newPassword: this.changeForm.value.password,
        token: this.token
      }).toPromise();
      if (result.succeeded) {
        this.toastr.info('Password change Successful', 'Success');
        await this.router.navigate(['/account/login']);
      }
      else  {
        this.toastr.warning('An unknown error occured, please try again later.', 'Failed');
      }
    }
  }

}
