import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ApiModule, Configuration} from 'api-client';
import {ErrorInterceptor} from '../helpers/error.interceptor';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {JwtInterceptor} from '../helpers/jwt.interceptor';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UnauthorisedContainerComponent} from './common/unauthorised-container/unauthorised-container.component';
import {AuthorisedContainerComponent} from './common/authorised-container/authorised-container.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormsModule} from '@angular/forms';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './home/home.component';
import { VerifyAccountComponent } from './verify-account/verify-account.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SidebarComponent } from './common/authorised-container/sidebar/sidebar.component';
import { NavbarComponent } from './common/authorised-container/navbar/navbar.component';
import { PageHeaderComponent } from './common/authorised-container/page-header/page-header.component';


import { AcceptInviteComponent } from './accept-invite/accept-invite.component';
import {DataTablesModule} from 'angular-datatables';


import { AccountTransactionsComponent } from './account-transactions/account-transactions.component';
import { AccountOpeningComponent } from './account-opening/account-opening.component';






@NgModule({
  declarations: [
    AppComponent,
    UnauthorisedContainerComponent,
    AuthorisedContainerComponent,
    LoginComponent,
    RegisterComponent,
    ResetPasswordComponent,
    HomeComponent,
    VerifyAccountComponent,
    ChangePasswordComponent,
    SidebarComponent,
    NavbarComponent,
    PageHeaderComponent,

    AcceptInviteComponent,

    AccountTransactionsComponent,
    AccountOpeningComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DataTablesModule,
    ToastrModule.forRoot(),
    // Rider reports this as an error but it is not
    ApiModule.forRoot(() => new Configuration({
      basePath: 'http://localhost:5000'
    })),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
