import {Component, OnInit} from '@angular/core';
import {AuthenticationService as APIAuthenticationService} from 'api-client';
import {IdentityResult} from '../../../projects/api-client/src';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {MatchPassword} from '../models/validation-helpers';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public pwdPattern = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
  public registerForm = this.fb.group({
    UserName: ['', [Validators.required, Validators.minLength(3)]],
    emailAddress: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required], Validators.pattern(this.pwdPattern)],
    confirmPassword: ['', [Validators.required]]
  }, {
    validator: MatchPassword('password', 'confirmPassword')
  });

  public get username(): AbstractControl {
    return this.registerForm.get('UserName');
  }

  public get emailAddress(): AbstractControl {
    return this.registerForm.get('emailAddress');
  }

  public get password(): AbstractControl {
    return this.registerForm.get('password');
  }

  public get confirmPassword(): AbstractControl {
    return this.registerForm.get('confirmPassword');
  }

  constructor(private authService: APIAuthenticationService, private toastr: ToastrService,
              private fb: FormBuilder, private router: Router) {
  }

  public async ngOnInit(): Promise<void> {
  }

  public async register(): Promise<void> {
    const result: IdentityResult = await this.authService.apiAuthenticationRegisterPost({
      userName: this.registerForm.value.UserName,
      emailAddress: this.registerForm.value.emailAddress,
      password: this.registerForm.value.password
    }).toPromise();
    console.log(result);

    if (result.succeeded) {
      this.toastr.info('Registration Successful,Please verify your email', 'Success');
     // await this.router.navigate(['/account/create']);
    }
  }
}
