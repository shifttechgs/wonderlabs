import {Component, HostBinding} from '@angular/core';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
declare var $: any;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'VirtualDoctorPlatform';
  constructor(private router: Router) {

    router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        $('.form-input-styled').uniform();
      }
    });

  }

}
