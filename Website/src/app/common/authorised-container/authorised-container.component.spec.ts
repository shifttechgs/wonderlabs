import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorisedContainerComponent } from './authorised-container.component';

describe('AuthorisedContainerComponent', () => {
  let component: AuthorisedContainerComponent;
  let fixture: ComponentFixture<AuthorisedContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorisedContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorisedContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
