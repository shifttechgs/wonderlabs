import {Component, OnInit, AfterViewInit, Renderer2} from '@angular/core';
import {App} from './template-helper';

@Component({
  selector: 'app-authorised-container',
  templateUrl: './authorised-container.component.html',
  styleUrls: ['./authorised-container.component.scss']
})
export class AuthorisedContainerComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  public ngOnInit(): void {
    App.initBeforeLoad();
    App.initCore();
  }

  public ngAfterViewInit(): void {
    App.initAfterLoad();
  }

}
