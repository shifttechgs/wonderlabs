import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../../services/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  username: string;
  constructor(private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    //console.log(localStorage.getItem('currentUser'));
    const user = JSON.parse(sessionStorage.getItem('currentUser'));
    console.log(user);
    this.username = user.userName;
  }


  public logout(): void {
    this.authService.logout();
  }

}
