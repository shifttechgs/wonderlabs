import {Component, OnInit, Renderer2, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-unauthorised-container',
  templateUrl: './unauthorised-container.component.html',
  styleUrls: ['./unauthorised-container.component.scss']
})
export class UnauthorisedContainerComponent implements OnInit, OnDestroy {

  constructor(private renderer: Renderer2) {
    this.renderer.addClass(document.body, 'bg-slate-800');
  }

  public ngOnInit(): void {

  }

  public ngOnDestroy(): void {
    this.renderer.removeClass(document.body, 'bg-slate-800');
  }

}
