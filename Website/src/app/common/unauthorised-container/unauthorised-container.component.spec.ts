import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnauthorisedContainerComponent } from './unauthorised-container.component';

describe('UnauthorisedContainerComponent', () => {
  let component: UnauthorisedContainerComponent;
  let fixture: ComponentFixture<UnauthorisedContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthorisedContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthorisedContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
