import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {
  AccountOpeningService,
  AuthenticationService as APIAuthenticationService,
  TransactionsService
} from "api-client";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
 public transactions: any;
  public user: any;
  public userAcc: any;
  public userAccounts: any;
  public accounts: any;
  public myTransactions: [];
  accountBal:any;
  constructor(private authService: AuthenticationService,
              private autheService: APIAuthenticationService,
               private transService: TransactionsService,


               private accountService: AccountOpeningService ,) { }

  public async ngOnInit(): Promise<void> {

    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
   // console.log(this.user);
    this.userAcc = await this.autheService.apiAuthenticationUserEmailGet(this.user.email).toPromise();
    this.transactions = await this.transService.apiTransactionsGetAccountTransactionsAccountIdGet(this.userAcc.id).toPromise();

    this.accounts= await this.accountService.apiAccountOpeningGetAccountsAccountNumberGet(this.userAcc.id).toPromise();

    this.myTransactions = this.transactions[0].userTransaction;
    this.accountBal= this.transactions[0].accountBalance;
    this.userAccounts= this.accounts[0];


    console.log(this.userAccounts.accountName);
    console.log(this.accountBal);
   console.log(this.transactions[0].accountBalance);
    console.log(this.myTransactions);
   // this.myTransactions.forEach(item=> {
   //
   //    console.log(item.accountId );
   //  });


  }

  log_out(){
    this.authService.logout();
  }
}
