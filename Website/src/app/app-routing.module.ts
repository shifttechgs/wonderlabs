import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UnauthorisedContainerComponent} from './common/unauthorised-container/unauthorised-container.component';
import {AuthorisedContainerComponent} from './common/authorised-container/authorised-container.component';
import {AuthGuard} from '../services/guards/auth.guard';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {HomeComponent} from './home/home.component';
import {VerifyAccountComponent} from './verify-account/verify-account.component';
import {ChangePasswordComponent} from './change-password/change-password.component';


import {AcceptInviteComponent} from './accept-invite/accept-invite.component';

import {AccountTransactionsComponent} from "./account-transactions/account-transactions.component";
import {AccountOpeningComponent} from "./account-opening/account-opening.component";


const routes: Routes = [
  {
    path: 'account', component: UnauthorisedContainerComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginComponent},
      {path: 'register', component: RegisterComponent},
      {path: 'reset-password', component: ResetPasswordComponent},
      {path: 'ResetPassword/:email/:token', component: ChangePasswordComponent},
      {path: 'verifyAccount/:userId/:token', component: VerifyAccountComponent},
      {path: 'accept/:email/:token', component: AcceptInviteComponent}
    ]
  },
  {
    path: '', component: AuthorisedContainerComponent, canActivate: [AuthGuard],
    children: [
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {path: 'profile', component: HomeComponent},
      {path: 'settings', component: HomeComponent}
    ],
  },

  {
    path: 'account', component: AuthorisedContainerComponent, canActivate: [AuthGuard],
    children: [
      {path: 'transactions', component: AccountTransactionsComponent},

    ]
  },

  {
    path: 'openAccount', component: AuthorisedContainerComponent, canActivate: [AuthGuard],
    children: [
      {path: 'newAccount', component: AccountOpeningComponent},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
