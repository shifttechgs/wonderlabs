import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthenticationService as APIAuthenticationService} from 'api-client';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  public resetForm = this.fb.group({
    emailAddress: ['', [Validators.required, Validators.email]]

  });
  constructor( private authService: APIAuthenticationService, private fb: FormBuilder, private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {
  }

  public async reset(): Promise<void> {

    if (this.resetForm.valid) {
      const result = await this.authService.apiAuthenticationForgotPasswordPost(this.resetForm.value.emailAddress).toPromise();

      if (result.succeeded) {
        this.toastr.info('Please check Your email ', 'Success');
        await this.router.navigate(['/account/login']);
      }
    }
  }

}
