import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
    rememberMe: ['']
  });

  constructor(private toastr: ToastrService, private authService: AuthenticationService, private fb: FormBuilder, private router: Router) {
  }

  public ngOnInit(): void {
  }
  public async login(): Promise<void> {
    const result = await this.authService.login(
      this.loginForm.get('username').value,
      this.loginForm.get('password').value,
      false
    );
    console.log(result);
    if (result) {
      this.router.navigate(['home']);
    }else{
      this.toastr.warning('Invalid login details', 'Error');
    }
  }
}
