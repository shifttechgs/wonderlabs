import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from "@angular/forms";
import {
  AccountOpeningService,
  AuthenticationService as APIAuthenticationService,
  TransactionsService
} from "api-client";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'app-account-transactions',
  templateUrl: './account-transactions.component.html',
  styleUrls: ['./account-transactions.component.scss']
})
export class AccountTransactionsComponent implements OnInit {


  public user: any;
  public userAcc: any;
  usernameId: string;
  accHolder:string;
  public newTransactionForm = this.fb.group({
    // AccountId: ['', [Validators.required, Validators.minLength(3)]],
    //  AccountNumber: ['', [Validators.required, Validators.minLength(3)]],
    transactionType: ['', [Validators.required, Validators.minLength(3)]],
    Amount: ['', [Validators.required, Validators.minLength(3)]]

  })
  // public get accountId(): AbstractControl {
  //   return this.newTransactionForm.get('AccountId');
  // }
  // public get accountNumber(): AbstractControl {
  //   return this.newTransactionForm.get('AccountNumber');
  // }
  public get transactionTypes(): AbstractControl {
    return this.newTransactionForm.get('transactionType');
  }
  public get amount(): AbstractControl {
    return this.newTransactionForm.get('Amount');
  }

  constructor(private authService: APIAuthenticationService,
              private accountService: AccountOpeningService ,
              private transService: TransactionsService,
              private toastr: ToastrService,
              private fb: FormBuilder, private router: Router) { }

  public async ngOnInit(): Promise<void> {

    console.log(sessionStorage.getItem('currentUser'));
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));
    console.log(this.user);
    this.userAcc = await this.authService.apiAuthenticationUserEmailGet(this.user.email).toPromise();
    console.log(this.userAcc.id);
  }

  public async newTransaction(): Promise<void> {
    const result: boolean = await this.transService.apiTransactionsCreateNewTransactionPost({
      accountId: this.userAcc.id,
     // accountNumber: this.userAcc.id,
      transactionType: this.newTransactionForm.value.transactionType,
      amount: this.newTransactionForm.value.Amount,

    }).toPromise();

    console.log(this.newTransactionForm.value.transactionType);
    console.log(result);

    if (result) {
      this.toastr.info('Transaction Created Successfully', 'Success');
      // await this.router.navigate(['/home']);
    }else{
      this.toastr.info('Transaction Failed ', 'Error');
    }
  }


}
