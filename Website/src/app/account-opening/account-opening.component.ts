import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, Validators} from "@angular/forms";
import {AccountOpeningService, AuthenticationService as APIAuthenticationService} from "api-client";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {IdentityResult} from "../../../projects/api-client/src";

@Component({
  selector: 'app-account-opening',
  templateUrl: './account-opening.component.html',
  styleUrls: ['./account-opening.component.scss']
})
export class AccountOpeningComponent implements OnInit {
  public user: any;
  public userAcc: any;
  usernameId: string;
  accHolder: string;

  public newAccountForm = this.fb.group({
    AccountName: ['', [Validators.required, Validators.minLength(3)]],
    AccountType: ['', [Validators.required, Validators.minLength(3)]],
    Amount: ['', [Validators.required, Validators.min(1000)]]

  })

  public get accountName(): AbstractControl {
    return this.newAccountForm.get('AccountName');
  }

  public get accountTypes(): AbstractControl {
    return this.newAccountForm.get('AccountType');
  }

  public get amount(): AbstractControl {
    return this.newAccountForm.get('Amount');
  }

  constructor(private authService: APIAuthenticationService,
              private accountService: AccountOpeningService,
              private toastr: ToastrService,
              private fb: FormBuilder, private router: Router) {
  }

  public async ngOnInit(): Promise<void> {

    console.log(sessionStorage.getItem('currentUser'));
    this.user = JSON.parse(sessionStorage.getItem('currentUser'));

    this.userAcc = await this.authService.apiAuthenticationUserEmailGet(this.user.email).toPromise();


  }


  public async openAccount(): Promise<void> {
    const result: boolean = await this.accountService.apiAccountOpeningCreateNewAccountPost({
      accountName: this.newAccountForm.value.AccountName,
      accountNumber: this.userAcc.id,
      accountType: this.newAccountForm.value.AccountType,
      accountBalance: this.newAccountForm.value.Amount,
    }).toPromise();
    console.log(result);

    if (result) {
      this.toastr.info('Account Created Successfully', 'Success');
      // await this.router.navigate(['/home']);
    } else {
      this.toastr.info('Account Creation Fail ', 'Error');
    }
  }
}

