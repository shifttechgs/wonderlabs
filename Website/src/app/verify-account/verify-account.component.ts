import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService as APIAuthenticationService} from 'api-client';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-verify-account',
  templateUrl: './verify-account.component.html',
  styleUrls: ['./verify-account.component.scss']
})
export class VerifyAccountComponent implements OnInit {

  constructor(private route: ActivatedRoute, private authService: APIAuthenticationService, private toastr: ToastrService , private router: Router) {

  }

  public async ngOnInit(): Promise<void> {
    await this.verifyAccount();
  }

  public async verifyAccount(): Promise<void> {
    const token = this.route.snapshot.paramMap.get('token');
    const user = this.route.snapshot.paramMap.get('userId');
    const result = await this.authService.apiAuthenticationConfirmEmailGet(user, token).toPromise();
    if (result.succeeded) {
      this.toastr.info('Account verified  Successful,Please login', 'Success');
       await this.router.navigate(['/account/create']);
    }
  }
}
