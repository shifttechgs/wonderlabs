import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService as APIAuthenticationService} from 'api-client';
import {ToastrService} from 'ngx-toastr';
import {IdentityResult} from '../../../projects/api-client/src';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {MatchPassword} from '../models/validation-helpers';

@Component({
  selector: 'app-accept-invite',
  templateUrl: './accept-invite.component.html',
  styleUrls: ['./accept-invite.component.scss']
})
export class AcceptInviteComponent implements OnInit {

  token: string;
  public pwdPattern = '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$';
  public registerForm = this.fb.group({
    UserName: ['', [Validators.required, Validators.minLength(3)]],
    emailAddress: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required], Validators.pattern(this.pwdPattern)],
    confirmPassword: ['', [Validators.required]]
  }, {
    validator: MatchPassword('password', 'confirmPassword')
  });

  public get username(): AbstractControl {
    return this.registerForm.get('UserName');
  }

  public get emailAddress(): AbstractControl {
    return this.registerForm.get('emailAddress');
  }

  public get password(): AbstractControl {
    return this.registerForm.get('password');
  }

  public get confirmPassword(): AbstractControl {
    return this.registerForm.get('confirmPassword');
  }


  constructor(private route: ActivatedRoute, private authService: APIAuthenticationService, private toastr: ToastrService,
              private fb: FormBuilder, private router: Router) {
    const email = this.route.snapshot.paramMap.get('email');
    console.log(email);
    this.registerForm.patchValue({
      emailAddress: email
    });
  }


  public async ngOnInit(): Promise<void> {
  }

  public async Accept(): Promise<void> {
    //  const token = this.route.snapshot.paramMap.get('token');
    console.log(this.registerForm.value.UserName);
    console.log(this.registerForm.value.password);
    const result: IdentityResult = await this.authService.apiAuthenticationRegisterTokenPost(this.route.snapshot.paramMap.get('token'),
      {
        userName: this.registerForm.value.UserName,
        emailAddress: this.route.snapshot.paramMap.get('email'),
        password: this.registerForm.value.password
      }).toPromise();
    console.log(result);
    if (result.succeeded) {
      this.toastr.info('Account registered Successfully,Please login', 'Success');
      await this.router.navigate(['/account/login']);
    }
  }

}
