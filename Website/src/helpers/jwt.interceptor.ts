import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from "rxjs/operators";
import {UserDataStoreService} from "../services/user-data-store.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private userDataStore: UserDataStoreService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const currentUser = this.userDataStore.currentUserValue;
    if (currentUser && currentUser.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUser.token}`
        }
      });
    }
    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          const jwt = event.headers.get('Authorization');
          if (jwt && jwt !== `Bearer ${currentUser.token}`) {
            this.userDataStore.RefreshJWT(jwt);
          }
        }
      })
    );
  }
}
