import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AccessPolicyEnum, DefaultAccessPolicies, PermissionEnum} from './permissions';
import {UserDataStoreService} from '../user-data-store.service';
import {ToastrService} from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private userDataStore: UserDataStoreService,
    private router: Router,
    private toastr: ToastrService
  ) {

  }

  public async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    if (this.userDataStore.currentUserValue) {
      let hasPermissions = await this.hasRequiredPermissions(route.data.permissions, route.data.policies);
      if ( route.data.accountType !== undefined){
          hasPermissions = false;
      }
      if ( !hasPermissions){
        this.toastr.warning('You do not have the required permissions to view this page, please contact your administrator if you require access.', 'Warning');
      }
      return hasPermissions;
    }
    // not logged in so redirect to login page with the return url
    await this.router.navigate(['account', 'login']);
    return false;
  }


  public async hasRequiredPermissions(permissions: PermissionEnum[], policies: AccessPolicyEnum[]): Promise<boolean> {
    const checker = (arr, target) => target.every(v => arr.includes(v));

    let requiredPermissions = [];
    if (permissions) {
      requiredPermissions = [...permissions];
    }

    if (policies) {
      for (const policy of policies) {
        requiredPermissions = [...requiredPermissions, ...DefaultAccessPolicies[policy]];
      }
    }

    if (requiredPermissions && !checker(this.userDataStore.currentUserPermissions, requiredPermissions)) {
      // role not authorised so redirect to home page
      await this.router.navigate(['/']);
      return false;
    }
    // authorised so return true
    return true;
  }
}
