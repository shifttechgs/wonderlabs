﻿export enum PermissionEnum {
  //Roles
  AddRole="add.role",
  DeleteRole="delete.role",
  EditRole="edit.role",
  ViewRole="view.role"
}

export const PermissionEnumMapping: Record<PermissionEnum, string> = {
  [PermissionEnum.AddRole]: "Add Roles",
  [PermissionEnum.DeleteRole]: "Delete Roles",
  [PermissionEnum.EditRole]: "Edit Roles",
  [PermissionEnum.ViewRole]: "View Roles",
};

export enum AccessPolicyEnum {
  RolesManagement="RolesManagement"
}

export const DefaultAccessPolicies: Record<AccessPolicyEnum, PermissionEnum[]> = {
  [AccessPolicyEnum.RolesManagement]: [PermissionEnum.EditRole, PermissionEnum.DeleteRole, PermissionEnum.AddRole, PermissionEnum.ViewRole]
};


