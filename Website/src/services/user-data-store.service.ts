import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import * as jwt_decode from 'jwt-decode';
import {AccessPolicyEnum, DefaultAccessPolicies, PermissionEnum} from './guards/permissions';
import {LoggedInUserViewModel} from '../../projects/api-client/src';


@Injectable({
  providedIn: 'root'
})
export class UserDataStoreService {

  private userPermissionsSubject: BehaviorSubject<string[]>;

  public get currentUserPermissions(): string[] {
    return this.userPermissionsSubject.value;
  }


  private currentUserSubject: BehaviorSubject<LoggedInUserViewModel>;
  public currentUser: Observable<LoggedInUserViewModel>;

  public get currentUserValue(): LoggedInUserViewModel {
    return this.currentUserSubject.value;
  }

  constructor() {
    this.userPermissionsSubject = new BehaviorSubject<string[]>(JSON.parse(sessionStorage.getItem('userPermissions')));
    this.currentUserSubject = new BehaviorSubject<LoggedInUserViewModel>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }


  public UpdateUser(user: LoggedInUserViewModel): void {
    sessionStorage.setItem('currentUser', JSON.stringify(user));
    const decodedJWT = jwt_decode(user.token);
    const permissions: string[] = [];
    if (!!decodedJWT.AccessPermission) {
      if (Array.isArray(decodedJWT.AccessPermission)) {
        permissions.push(...decodedJWT.AccessPermission);
      } else {
        permissions.push(decodedJWT.AccessPermission);
      }
    }
    sessionStorage.setItem('userPermissions', JSON.stringify(permissions));
    this.userPermissionsSubject.next(permissions);
    this.currentUserSubject.next(user);
  }

  public RefreshJWT(jwt: string): void {
    const updatedUser = JSON.parse(sessionStorage.getItem('currentUser'));
    updatedUser.token = jwt.replace('Bearer ', '');
    sessionStorage.setItem('currentUser', JSON.stringify(updatedUser));
    this.currentUserSubject.next(updatedUser);
  }

  public Clear(): void {
    sessionStorage.removeItem('currentUser');
    sessionStorage.removeItem('userPermissions');
    this.userPermissionsSubject.next(null);
    this.currentUserSubject.next(null);
  }

  public hasRequiredPermissions(permissions: PermissionEnum[], policies: AccessPolicyEnum[]): boolean {
    const checker = (arr, target) => target.every(v => arr.includes(v));

    let requiredPermissions = [];
    if (permissions) {
      requiredPermissions = [...permissions];
    }

    if (policies) {
      for (const policy of policies) {
        requiredPermissions = [...requiredPermissions, ...DefaultAccessPolicies[policy]];
      }
    }

    if (requiredPermissions && !checker(this.currentUserPermissions, requiredPermissions)) {
      return false;
    }
    // authorised so return true
    return true;
  }

}
