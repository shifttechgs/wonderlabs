import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {UserDataStoreService} from './user-data-store.service';
import {AuthenticationService as APIAuthenticationService, LoginViewModel} from 'api-client';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  constructor(
    private router: Router,
    private userDataStore: UserDataStoreService,
    private APIAuthenticationService: APIAuthenticationService
  ) {

  }

  public async login(userName: string, password: string, rememberMe: boolean): Promise<boolean> {
    const loginViewModel: LoginViewModel = {
      password : password,
      rememberMe : rememberMe,
      userName : userName
    };

    const result = await this.APIAuthenticationService.apiAuthenticationLoginPost(loginViewModel).toPromise();

    if (result) {
      this.userDataStore.UpdateUser(result);
      return true;
    }
    return false;
  }

  public async logout(): Promise<void> {
    this.userDataStore.Clear();
    await this.router.navigate(['account', 'login']);
  }


}
