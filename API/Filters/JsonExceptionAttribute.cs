﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Models;

namespace API.Filters
{
    public class JsonExceptionAttribute: ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception != null)
            {
                string message = "An unknown error occured, if this problem persists please contact your system administrator.";
                if (context.Exception.GetType() == typeof(ApiException))
                {
                    message = context.Exception.Message;
                    context.HttpContext.Response.StatusCode = (int)((ApiException)context.Exception).HttpStatusCode;
                }
                else
                {
                    context.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                }

                context.Result = new JsonResult(new
                {
                    Success = false,
                    Error = message
                });
                context.ExceptionHandled = true;
            }
        }
    }
}