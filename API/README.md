﻿# BasePlatform

## WebApi Project Setup
### Install .net core 3.1
Download and install the latest version of [.net core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)

## General Notes

### Included Library's
 - [Swashbuckle](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.1&tabs=visual-studio)

This project makes use of swagger through swashbuckle, the intention of this is for code generation of an angular client.
When debugging the api in development mode navigate to the following URL to access the swagger endpoint.

You can download the latest API definition file (swagger.json), this is the file that is used for code generation.
More details and instructions for code generation can be found in the [Website Readme](../Website/Readme.md).

[Swagger Endpoint](https://localhost:5001/swagger/v1/index.html)
[Swagger Definition](https://localhost:5001/swagger/v1/swagger.json)
