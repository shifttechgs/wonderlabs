﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.ViewModels;
using Services;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using SendGrid.Helpers.Mail;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly SignInManager<User> _signInManager;
        private readonly IUserAuthenticationService _userAuthenticationService;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;


        public AuthenticationController(IUserAuthenticationService userAuthenticationService,
            SignInManager<User> signInManager, IConfiguration configuration, UserManager<User> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _userAuthenticationService = userAuthenticationService;
            _configuration = configuration;
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Ok(new {succeeded = true});
        }

        [HttpPost("login")]
        public async Task<LoggedInUserViewModel> Login([FromBody] LoginViewModel model)
        {
            return await _userAuthenticationService.Authenticate(model);
        }


        [HttpPost("register")]
        public async Task<IdentityResult> Register([FromBody] RegisterViewModel model)
        {
            return await _userAuthenticationService.RegisterUser(model);
        }

       [HttpPost("register/{token}")]
       public async Task<IdentityResult> Register([FromBody] RegisterViewModel model, Guid token)
       {
         return await _userAuthenticationService.RegisterUserWithInvite(model,token);
       }
       
        [HttpGet("ConfirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string user, string token)
        {
            await _userAuthenticationService.ConfirmEmailAsync(user, token);


            //return successful json response
            return Ok(new {succeeded = true});
        }

        [HttpPost("ForgotPassword")]
        public async Task<IActionResult> ForgotPassword(string emailAddress)
        {
            await _userAuthenticationService.SendResetPasswordLink(emailAddress);

            return Ok(new {succeeded = true});
            //return json response
        }

        [HttpPost("ResetPassword")]
        public async Task<IdentityResult> ResetPassword([FromBody] ResetPasswordViewModel model)
        {
            return await _userAuthenticationService.ResetPasswordAsync(model);
        }

        [HttpPost("Invite")]
        public async Task<IActionResult> SendInvite(InviteViewModel model)
        {
            await _userAuthenticationService.SendInviteEmail(model);

            return Ok(new {succeeded = true});
            //return json response
        }
        
        [HttpGet("User/{email}")]
        public async Task<ActionResult<UserViewModel>> GetUser([FromRoute] string email)
        {
            var result = await _userAuthenticationService.GetUserByEmail(email);
            return Ok(result);
        }


        



    }

}