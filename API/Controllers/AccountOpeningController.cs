﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DataRepository.Repositorys;
using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.ViewModels;
using MongoDB.Bson;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountOpeningController : ControllerBase
    {
        // GET
        private readonly IAccountOpeningRepository _iAccountOpeningRepository;
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly IMapper _mapper;

        public AccountOpeningController(IAccountOpeningRepository iAccountOpeningRepository,
            IMapper mapper, ITransactionsRepository transactionsRepository)
        {
            _iAccountOpeningRepository = iAccountOpeningRepository;
            _mapper = mapper;
            _transactionsRepository = transactionsRepository;
        }

        [HttpPost("CreateNewAccount")]
        public async Task<bool> Create(AccountOpeningViewModel model)
        {
            // var accountOpening = _mapper.Map<AccountOpening>(model);

            var accOpening = new AccountOpening()
            {
                AccountBalance = model.AccountBalance,
                AccountName = model.AccountName,
                AccountNumber = model.AccountNumber,
                AccountType = model.AccountType,
                DateOpened = DateTime.Now
            };

            await _iAccountOpeningRepository.InsertOneAsync(accOpening);


            // create trans table 
            var transaction = new Transactions();

            transaction.AccountBalance = accOpening.AccountBalance;
            transaction.AccountId = accOpening.AccountNumber;
            transaction.LastDateOfTransaction = DateTime.Now;
            transaction.UserTransaction = new List<UserTransactions>()
            {
                new UserTransactions
                {
                    Amount = accOpening.AccountBalance,
                    TransactionDate = DateTime.Now,
                    TransactionType = "Initial Deposit",
                    AccountBalance = transaction.AccountBalance
                }
            };

            await _transactionsRepository.InsertOneAsync(transaction);

            return true;
        }

        [HttpGet("getAccounts")]
        public async Task<List<AccountOpeningViewModel>> GetAllAccounts()
        {
            var result = await _iAccountOpeningRepository.FindAllAccounts();
            var accounts = new List<AccountOpeningViewModel>();
            foreach (var account in result)
            {
                accounts.Add(_mapper.Map<AccountOpeningViewModel>(account));
            }
        
            return accounts;
        }
        
        [HttpGet("getAccounts/{accountNumber}")]
        public async Task<List<AccountOpeningViewModel>> GetAllAccounts(string accountNumber)
        {
            var result = await _iAccountOpeningRepository.FindAccount( accountNumber);
            var accounts = new List<AccountOpeningViewModel>();
            foreach (var account in result)
            {
                accounts.Add(_mapper.Map<AccountOpeningViewModel>(account));
            }

            return accounts;
        }

        [HttpGet("getAccount/{id}")]
        public async Task<AccountOpeningViewModel> GetAccount(string id)
        {
            var result = await _iAccountOpeningRepository.FindByIdAsync(id);
            return _mapper.Map<AccountOpeningViewModel>(result);
        }
    }
}