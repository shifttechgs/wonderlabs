﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Filters;
using AspNetCore.Identity.Mongo.Model;
using AutoMapper;
using DataRepository;
using DataRepository.Repositorys;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.ViewModels;
using Services;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Models;
using MongoDB.Bson;
using MongoDB.Driver;
using SendGrid.Helpers.Mail;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DeveloperController : ControllerBase
    {
        private readonly IClientRepository _clientRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;
        private readonly IUserAuthenticationService _userAuthenticationService;
        private RoleManager<Role> _roleManager;

        public DeveloperController(IClientRepository clientRepository, IMapper mapper,
            IUserAuthenticationService userAuthenticationService, IUserRepository userRepository,
            RoleManager<Role> roleManager)
        {
            _clientRepository = clientRepository;
            _userRepository = userRepository;
            _userAuthenticationService = userAuthenticationService;
            _mapper = mapper;
            _roleManager = roleManager;
        }


        [HttpGet("AddInitialRoles")]
        public async Task<IActionResult> AddInitialRoles()
        {
            await _userAuthenticationService.UpdateUser(new UserViewModel()
            {
                Id = "5efc7fbcb4354b0b5cfa96a2",
                EmailAddress = "mike@lunasoft.co.za",
                UserName = "mike",
                Roles = new List<UserRoleViewModel>()
                {
                    new UserRoleViewModel()
                    {
                        Id = "5f0c4bfe76fea99db873622f",
                        Name = "Admin"
                    },
                    new UserRoleViewModel()
                    {
                        Id = "5f0c4d21eb4e554ac0ee2a83",
                        Name = "ClientUser"
                    }
                }
            });

            // InsertRoleWithClaimsViewModel roleWithClaimsViewModel = new InsertRoleWithClaimsViewModel();
            //
            //
            // roleWithClaimsViewModel.Role = new RoleViewModel()
            // {
            //     Name = "ClientUser",
            //     Description = "Client User role for platform"
            // };
            //
            // roleWithClaimsViewModel.Claims = new List<InsertClaimViewModel>();
            //
            // roleWithClaimsViewModel.Claims.Add(new InsertClaimViewModel()
            // {
            //     ClaimType = "Permission",
            //     ClaimValue = "configureappointments.create"
            // });
            //
            // roleWithClaimsViewModel.Claims.Add(new InsertClaimViewModel()
            // {
            //     ClaimType = "Permission",
            //     ClaimValue = "configureappointments.read"
            // });
            //
            // roleWithClaimsViewModel.Claims.Add(new InsertClaimViewModel()
            // {
            //     ClaimType = "Permission",
            //     ClaimValue = "configureappointments.update"
            // });
            //
            // roleWithClaimsViewModel.Claims.Add(new InsertClaimViewModel()
            // {
            //     ClaimType = "Permission",
            //     ClaimValue = "configureappointments.delete"
            // });
            //
            // await _userAuthenticationService.InsertRoleAndPermissions(roleWithClaimsViewModel);


            return Ok(new {result = "Done"});
        }


        [HttpGet("TestAdminRole")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> TestAdminRole()
        {
            return Ok(new {result = "You have access"});
        }
        
        [HttpGet("TestClientUserRole")]
        [Authorize(Roles = "ClientUser")]
        public async Task<IActionResult> TestClientUserRole()
        {
            return Ok(new {result = "You have access"});
        }
    }
}