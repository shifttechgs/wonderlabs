﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DataRepository.Repositorys;
using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.ViewModels;

namespace API.Controllers
{
   
    [ApiController]
    [Route("api/[controller]")]
    
    public class AccountTypesController  : ControllerBase
    {
        // GET
        private readonly IAccountTypesRepository _iAccountTypesRepository;
        private readonly IMapper _mapper;

        public AccountTypesController(IAccountTypesRepository iAccountTypesRepository,
            IMapper mapper)
        {
            _iAccountTypesRepository = iAccountTypesRepository;
            _mapper = mapper;
        }
        
        [HttpPost("CreateNewAccountType")]
        public async Task<ActionResult> Create(AccountTypesViewModel model)
        {
            var acc_type = _mapper.Map<AccountTypes>(model);

          
            await _iAccountTypesRepository.InsertOneAsync(acc_type);

            return Ok(new {result = "Done"});
        }
        
        [HttpGet("getAccountTypes")]
        public async Task<List<AccountTypesViewModel>> GetAllAccountTypes()
        {
            var result = await _iAccountTypesRepository.FindAllAccountTypes();
            var account_types = new List<AccountTypesViewModel>();
            foreach (var account_type in result)
            {
                account_types.Add(_mapper.Map<AccountTypesViewModel>(account_type));
            }

            return account_types;
        }
        
        [HttpGet("getAccountType/{id}")]
        public async Task<AccountTypesViewModel> GetAccountType(string id)
        {
            var result = await _iAccountTypesRepository.FindByIdAsync(id);
            return _mapper.Map<AccountTypesViewModel>(result);
        }
    }
}