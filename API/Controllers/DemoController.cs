﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Filters;
using AspNetCore.Identity.Mongo.Model;
using AutoMapper;
using DataRepository;
using DataRepository.Repositorys;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.ViewModels;
using Services;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Models;
using MongoDB.Bson;
using MongoDB.Driver;
using SendGrid.Helpers.Mail;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DemoController : ControllerBase
    {

        private readonly IMongoRepository<Job> _demoRepository;


        public DemoController(IMongoRepository<Job> demoRepository)
        {
            _demoRepository = demoRepository;
        }



        [HttpPost("create")]
        public async Task<IActionResult> Create()
        {
            _demoRepository.InsertOne(new Job()
            {
                Property1 = "prop1",
                Property2 = "prop2",
                Progress = 50
            });
            return Ok(new {result = "Done"});
        }
    }
}