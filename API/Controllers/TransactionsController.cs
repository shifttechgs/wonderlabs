﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using DataRepository.Repositorys;
using Microsoft.AspNetCore.Mvc;
using Models.Domain;
using Models.ViewModels;
using Services;

namespace API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        // GET
        private readonly IUserAuthenticationService _userAuthenticationService;
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly IAccountOpeningRepository _accountOpeningRepository;
        private readonly IMapper _mapper;

        public TransactionsController(ITransactionsRepository transactionsRepository,
            IMapper mapper, IAccountOpeningRepository accountOpeningRepository,
            IUserAuthenticationService authenticationService)
        {
            _accountOpeningRepository = accountOpeningRepository;
            _transactionsRepository = transactionsRepository;
            _userAuthenticationService = authenticationService;
            _mapper = mapper;
        }

        [HttpPost("CreateNewTransaction")]
        public async Task<ActionResult> Create(TransactionsViewModel model)
        {
            if (model.TransactionType == "Deposit")
            {
                var account = await _accountOpeningRepository.FindOneAsync(x => x.AccountNumber == model.AccountId);
                account.AccountBalance = account.AccountBalance + model.Amount;
                await _accountOpeningRepository.ReplaceOneAsync(account);

                // update transaction 

                var transaction = await _transactionsRepository.FindOneAsync(x => x.AccountId == account.AccountNumber);
                transaction.AccountBalance = transaction.AccountBalance + model.Amount;
                var transactionHistory = new UserTransactions
                {
                    Amount = model.Amount,
                    TransactionDate = DateTime.Now,
                    TransactionType = model.TransactionType,
                    AccountBalance = transaction.AccountBalance
                };
                transaction.UserTransaction.Add(transactionHistory);
                await _transactionsRepository.ReplaceOneAsync(transaction);
            }
            else
            {
                var account = await _accountOpeningRepository.FindOneAsync(x => x.AccountNumber == model.AccountId);

                if (account.AccountBalance > 1000)
                {
                    account.AccountBalance = account.AccountBalance + model.Amount;
                    await _accountOpeningRepository.ReplaceOneAsync(account);
                }
                // update transaction 

                var transaction = await _transactionsRepository.FindOneAsync(x => x.AccountId == account.AccountNumber);
                transaction.AccountBalance = transaction.AccountBalance - model.Amount;
                var transactionHistory = new UserTransactions
                {
                    Amount = model.Amount,
                    TransactionDate = DateTime.Now,
                    TransactionType = model.TransactionType,
                    AccountBalance = transaction.AccountBalance
                };
                transaction.UserTransaction.Add(transactionHistory);
                await _transactionsRepository.ReplaceOneAsync(transaction);
            }

            return Ok(new {result = "Done"});
        }

        [HttpGet("getTransactions")]
        public async Task<List<TransactionsViewModel>> GetAllTransactions()
        {
            var result = await _transactionsRepository.FindAllTransactions();
            var transactions = new List<TransactionsViewModel>();
            foreach (var transaction in result)
            {
                transactions.Add(_mapper.Map<TransactionsViewModel>(transaction));
            }

            return transactions;
        }

        [HttpGet("getTransaction/{id}")]
        public async Task<TransactionsViewModel> GetTransaction(string id)
        {
            var result = await _transactionsRepository.FindByIdAsync(id);
            return _mapper.Map<TransactionsViewModel>(result);
        }

        [HttpGet("getAccountTransactions/{accountId}")]
        public async Task<List<TransactionsViewModel>> GetAllAccountTransactions(string accountId)
        {
            var result = await _transactionsRepository.FindAccountTransactions(accountId);
            var transactions = new List<TransactionsViewModel>();
            foreach (var transaction in result)
            {
                transactions.Add(_mapper.Map<TransactionsViewModel>(transaction));
            }

            return transactions;
        }
    }
}