﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace Models.Domain
{
    [BsonCollection("Clients")]
    public class Client: Document
    {
        
        
        
        public string Name { get; set; }
        public string Subdomain { get; set; }
        public ObjectId AdminUser { get; set; }
        public List<ObjectId> Invite { get; set; }
        
        
    }
   
    
}