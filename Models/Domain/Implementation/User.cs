﻿using System;
using System.Collections.Generic;
using AspNetCore.Identity.Mongo.Model;
using MongoDB.Bson;

namespace Models.Domain
{
    [BsonCollection("Users")]
    public class User : MongoUser, IDocument 
    {
        public DateTime CreatedAt => Id.CreationTime;
        public List<ObjectId> Clients { get; set; }

    }
}