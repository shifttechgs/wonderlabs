﻿using System;
using MongoDB.Bson;

namespace Models.Domain
{
    public abstract class Document : IDocument
    {
        public ObjectId Id { get; set; }

        public DateTime CreatedAt => Id.CreationTime;
        
       
                
      
    }
}