﻿namespace Models.Domain
{
    public class SMTPSettings: ISMTPSettings  
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string SMTPAddress { get; set; }
        public int Port { get; set; }
        public string EncryptionMethod { get; set; }
    }
}