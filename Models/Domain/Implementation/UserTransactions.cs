﻿using System;

namespace Models.Domain
{
    public class UserTransactions
    {
        public decimal Amount { get; set; }
        public decimal AccountBalance { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionType { get; set; }
    }
}