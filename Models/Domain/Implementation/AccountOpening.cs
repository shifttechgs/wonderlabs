﻿using System;

namespace Models.Domain
{
    [BsonCollection("AccountOpening")]
    public class AccountOpening : Document
    {
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public decimal AccountBalance { get; set; }
        public DateTime DateOpened { get; set; }
    }
}