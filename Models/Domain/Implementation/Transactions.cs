﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;

namespace Models.Domain
{
    [BsonCollection("Transactions")]
    public class Transactions :Document
    {
        public string AccountId { get; set; }
        public List<UserTransactions> UserTransaction { get; set; }
        public decimal AccountBalance { get; set; }
        public DateTime LastDateOfTransaction { get; set; }
      
        public DateTime CreatedAt { get; }
    }

  
}