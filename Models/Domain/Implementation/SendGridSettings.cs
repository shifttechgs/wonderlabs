﻿namespace Models.Domain
{
    public class SendGridSettings : ISendGridSettings
    {
        public string ApiKey { get; set; }
        public SendGridTemplate[] Templates { get; set; }
        public string FromEmailAddress { get; set; }
        public string FromName { get; set; }
    }
}