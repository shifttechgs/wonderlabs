﻿namespace Models.Domain
{
    public class SendGridTemplate
    {
        public string Name { get; set; }
        public string TemplateId { get; set; }
    }
}