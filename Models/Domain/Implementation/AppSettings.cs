﻿namespace Models.Domain
{
    public class AppSettings : IAppSettings
    {
        public string Secret { get; set; }
        public string[] CorsOrigins { get; set; }
        public string WebUrl { get; set; }
    }
}