﻿using System;
using MongoDB.Bson;

namespace Models.Domain
{
    [BsonCollection("Invites")]
    public class Invite : Document
    {
        
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string UserEmail { get; set; }
        public bool Accepted { get; set; }
        public Guid Token { get; set; }
        public ObjectId ClientId { get; set; }
    }
}