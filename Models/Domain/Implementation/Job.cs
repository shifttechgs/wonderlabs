﻿namespace Models.Domain
{
    [BsonCollection("Jobs")]
    public class Job: Document
    {
        public string Property1 { get; set; }
        public string Property2 { get; set; }

        public int Progress { get; set; }
    }
}