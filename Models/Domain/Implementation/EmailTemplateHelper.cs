﻿namespace Models.Domain
{
    public static class EmailTemplateHelper
    {
        public static string ForgotPassword = "ForgotPassword";
        public static string ConfirmEmail = "ConfirmEmail";
        public static string InviteClient = "Invite";
    }
}