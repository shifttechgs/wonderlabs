﻿﻿using AspNetCore.Identity.Mongo.Model;
 using Microsoft.AspNetCore.Identity;

namespace Models.Domain
{
    public class Role : MongoRole
    {
        public string Description { get; set; }

        
        public Role(): base()
        {
        }
        public Role(string name): base(name)
        {
        }
        public Role(string name, string description): base(name)
        {
            this.Description = description;
        }
    }
}