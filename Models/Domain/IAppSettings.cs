﻿namespace Models.Domain
{
    public interface IAppSettings
    {
        string Secret { get; set; }
        string[] CorsOrigins { get; set; }
        string WebUrl { get; set; }
    }
}