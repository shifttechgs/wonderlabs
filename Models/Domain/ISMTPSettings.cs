﻿namespace Models.Domain
{
    public interface ISMTPSettings
    {
        string Email { get; set; }
        string Password { get; set; }
        string SMTPAddress { get; set; }
        int Port { get; set; }
        string EncryptionMethod { get; set; }
    }
}