﻿namespace Models.Domain
{
    public interface ISendGridSettings
    {
        string ApiKey { get; set; }
        SendGridTemplate[] Templates { get; set; }
        string FromEmailAddress { get; set; }
        string FromName { get; set; }
        
    }
}