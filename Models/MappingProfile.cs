﻿using AutoMapper;
using Models.Domain;
using Models.ViewModels;
using MongoDB.Bson;

namespace Models
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            
            //CreateMap<FromObject, DestinationObject>()
            //.ForMember(selectDestinationProperty(), getValueToAssignToDestination())
            
            CreateMap<Client, ClientViewModel>()
                .ForMember(x=> x.Id, opt=> opt.MapFrom(m=>m.Id.ToString()))
                .ForMember(x=>x.CreatedAt, opt=>opt.MapFrom(m=>m.Id.CreationTime))
                .ForMember(x=>x.AdminUser,opt=>opt.MapFrom(m=>m.AdminUser.ToString()) );

            CreateMap<ClientViewModel, Client>()
                .ForMember(x => x.Id, opt => opt.MapFrom(m => new ObjectId(m.Id)))
                .ForMember(x => x.AdminUser, opt => opt.MapFrom(m => new ObjectId(m.AdminUser)));


            CreateMap<Invite, InviteViewModel>()
               .ForMember(x => x.ClientId, opt => opt.MapFrom(m => m.ClientId.ToString()))
                .ForMember(x => x.Email, opt => opt.MapFrom(m => m.UserEmail));
            
            CreateMap<InviteViewModel, Invite>()
                .ForMember(x => x.ClientId, opt => opt.MapFrom(m => new ObjectId(m.ClientId)))
               .ForMember(x => x.UserEmail, opt => opt.MapFrom(m => m.Email));

            CreateMap<AccountOpeningViewModel, AccountOpening>().ReverseMap();
            CreateMap<AccountTypesViewModel, AccountTypes>().ReverseMap();
            CreateMap<TransactionsViewModel, Transactions>().ReverseMap();

        }
    }
}