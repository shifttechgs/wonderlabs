﻿﻿namespace Models.ViewModels
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        
    }
}//constructor(private authService: APIAuthenticationService) {