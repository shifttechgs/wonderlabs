﻿namespace Models.ViewModels
{
    public class InviteStatusViewModel
    {
       
        public string Id { get; set; }
        public string Token { get; set; }
    }
}