﻿using System;

namespace Models.ViewModels
{
    public class ClientViewModel
    {
        public string Id { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public string Name { get; set; }
        public string Subdomain { get; set; }
        public string AdminUser { get; set; }
    }

  
}