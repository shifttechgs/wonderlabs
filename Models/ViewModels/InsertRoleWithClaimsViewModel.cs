﻿﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Models.ViewModels
{
    public class InsertClaimViewModel
    {
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }

    public class RoleViewModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }

    public class InsertRoleWithClaimsViewModel
    {
        public RoleViewModel Role { get; set; }
        public List<InsertClaimViewModel> Claims { get; set; }
        public IdentityResult IdentityResult { get; set; }
    }
}