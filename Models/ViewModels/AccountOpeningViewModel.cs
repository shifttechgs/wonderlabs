﻿namespace Models.ViewModels
{
    public class AccountOpeningViewModel
    {
        public string Id { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string AccountType { get; set; }
        public decimal AccountBalance { get; set; }
        public string DateOpened { get; set; }
    }
}