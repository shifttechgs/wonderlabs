﻿using MongoDB.Bson;

namespace Models.ViewModels
{
    public class LoggedInUserViewModel
    {
        public ObjectId Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
    }
}