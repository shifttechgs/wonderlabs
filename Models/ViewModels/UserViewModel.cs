﻿﻿using System.Collections.Generic;

namespace Models.ViewModels
{
    public class UserRoleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }
        public string EmailAddress { get; set; }
        public string UserName { get; set; }
        
        public List<UserRoleViewModel> Roles { get; set; }
    }
}