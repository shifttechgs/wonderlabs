﻿namespace Models.ViewModels
{
    public class UpdateTransactionsViewModel
    {
        public string AccountId { get; set; }
        public string TransactionType { get; set; }
        public decimal Amount { get; set; }
        public decimal AccountBalance { get; set; }
        public string TransactionDate { get; set; }
    }
}