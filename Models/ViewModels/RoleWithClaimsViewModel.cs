﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Models.Domain;

namespace Models.ViewModels
{
    public class RoleWithClaimsViewModel
    {
        public Role Role { get; set; }
        public List<IdentityRoleClaim<string>> Claims { get; set; }
    }
}