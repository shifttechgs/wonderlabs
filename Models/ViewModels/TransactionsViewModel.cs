﻿using System;
using System.Collections.Generic;
using Models.Domain;

namespace Models.ViewModels
{
    public class TransactionsViewModel
    {
      
        public string AccountId { get; set; }
        public List<UserTransactions> UserTransaction { get; set; }
        public decimal AccountBalance { get; set; }
        public decimal Amount { get; set; }
        public string TransactionType { get; set; }
        public DateTime LastDateOfTransaction { get; set; }
    }
}