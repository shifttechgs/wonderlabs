﻿﻿namespace Models.ViewModels
{
    public class RegisterViewModel
    {
        public string UserName { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }

    }
}