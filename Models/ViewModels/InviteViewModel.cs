﻿using System.Collections.Generic;

namespace Models.ViewModels
{
    public class InviteViewModel
    {
        public string ClientId { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string Email { get; set; }
        public bool Accepted { get; set; }
    }
}