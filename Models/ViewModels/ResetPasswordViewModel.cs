﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Models.ViewModels
{
    public class ResetPasswordViewModel
    {
               
                public string Token { get; set; }
                
                public string EmailAddress { get; set; }
                
                public string NewPassword { get; set; }
        
        
               public string ConfirmPassword { get; set; }
            
    }
}