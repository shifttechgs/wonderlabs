﻿using System;
using System.Net;

namespace Models
{
    public class ApiException: Exception
    {

        public HttpStatusCode HttpStatusCode;
        public ApiException()
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }

        public ApiException(string message) : base(message)
        {
            HttpStatusCode = HttpStatusCode.BadRequest;
        }
        
        public ApiException(string message, HttpStatusCode httpStatusCode) : base(message)
        {
            this.HttpStatusCode = httpStatusCode;
        }
    }
}